import sys

print(
    """Hello, {{cookiecutter.author}}.
This is what to do next:
#-----
cd '{{cookiecutter.project_slug}}'
git init .
git remote add origin '{{cookiecutter.repo_url}}'
# create the repo if necessary
pipenv install --dev
pipenv run pre-commit install
pipenv shell
git add .
git commit -m'new project from template'
git tag -a 0.0.1 -m'initial release'
git push -u origin master
git push -u origin --tags
#-----
"""
)
sys.exit(0)
