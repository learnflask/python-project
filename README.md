
This is a cookie cutter template for a basic Python project.

Features:

 * `src` layout
 * manage dependencies using `pipenv`
 * `click` command line interface
 * Makefile for building and installing before tests (ensures that only packaged code gets tested)
 * `flake8` and `black` pre-commit hooks
 * MIT license
 * package version from `git describe --always --dirty`
    - use `git tag -a 0.0.0 -m'tag release'` to set version number
    - prevents uploading package containing uncommitted code
